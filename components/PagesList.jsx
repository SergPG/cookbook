import React, { useState } from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';

// Global Styles file
import { glStyles } from '../styles/styles';


const PagesList = ({ navigation , posts }) => {

  
  const Item = ({ page }) => (
    
  <TouchableOpacity       
        onPress={()=> navigation.navigate('Page',page)}
      >
  <View style={styles.item}>
  
    <Text style={glStyles.title}>{page.title}</Text>

    
  </View>
   </TouchableOpacity>
);

 
  // Item Post renderItem
  const renderItem = ({ item }) => (
    <Item page={item} />
  );


  return (
    <View >
      <View >
        <FlatList
          data={posts}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    </View>
  );
};

export default PagesList;

const styles = StyleSheet.create({
  container: {

  },
  item: {
    backgroundColor: '#80a31f',
    opacity: .6,
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 18,
  },
});
