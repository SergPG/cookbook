import React from 'react';

import HomeScreen from './screens/HomeScreen';
import PageScreen from './screens/PageScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { SimpleLineIcons } from '@expo/vector-icons';
import { Foundation } from '@expo/vector-icons';

const Stack = createNativeStackNavigator();

const Navigate = () => {

	return (
		<NavigationContainer>
			<Stack.Navigator>
				<Stack.Screen
					name="Home"
					component={HomeScreen}
					options={{ 
						headerStyle: {backgroundColor: '#3f0857',},
						headerTintColor: '#fff',
						headerTitle: (props) => <SimpleLineIcons name="home" size={24} color="#FFF" /> }}
				/>
				<Stack.Screen
					name="Page"
					component={PageScreen}
					options={{ 
						headerStyle: {backgroundColor: '#3f0857',},
						headerTintColor: '#fff',
						headerTitle: (props) => 
				<Foundation name="page" size={24} color="#fff" /> }}
				/>
			</Stack.Navigator>
		</NavigationContainer>
	);
};
export default Navigate;
