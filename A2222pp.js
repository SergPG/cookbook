import React, { useState } from 'react'
import { StyleSheet,View, SafeAreaView, StatusBar } from 'react-native';

// Global Styles file
import { glStyles } from './styles/styles';

//Fonts
import * as Font from 'expo-font';

import AppLoading from 'expo-app-loading';

// Site Main Navigate
import MainNavigate from './navigate';

const fonts = () => Font.loadAsync({
  'mt-bold-font': require('./assets/fonts/Montserrat/Montserrat-Bold.ttf'),
  'mt-light-font': require('./assets/fonts/Montserrat/Montserrat-Light.ttf'),
  'lobster-font': require('./assets/fonts/Lobster/Lobster-Regular.ttf'),
})

export default function App() {

  const [font, setFont] = useState(false);

  if (font) {

    return (
      <SafeAreaView style={glStyles.main}>
        <StatusBar
        animated={true}
        backgroundColor="#3f0857"
        barStyle={'light-content'}
        showHideTransition={'fade'}
        hidden= {false} />
        
        <MainNavigate />

      </SafeAreaView>
    );
  } else {
    return (
      <AppLoading
      startAsync={fonts}
      onFinish={() => setFont(true)}
      onError={console.warn}
    />
    )
  };
}

const styles = StyleSheet.create({
  container:{

  }
  
});
