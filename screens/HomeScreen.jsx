import React, { useState } from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';

// Global Styles file
import { glStyles } from '../styles/styles';

import PagesList from '../components/PagesList';


const HomeScreen = ({ navigation }) => {

  
  const [posts, setPosts] = useState([
    {
      userId: 1,
      id: 1,
      title: '2Sunt aut facere repellat provident occaecati',
      body: 'quia et suscipit nsuscipit recusandae consequuntur expedita et cum  nreprehenderit molestiae ut ut quas totam nnostrum rerum est autem sunt rem eveniet architecto'
    },
    {
      userId: 2,
      id: 2,
      title: ' User 2 Qui est esse',
      body: 'est rerum tempore vitae nsequi sint nihil reprehenderit dolor beatae ea dolores neque nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis nqui aperiam non debitis possimus qui neque nisi nulla'
    },
    {
      userId: 2,
      id: 3,
      title: 'Molestias quasi exercitationem repellat',
      body: ' User 2 id_3 et iusto sed quo iure nvoluptatem occaecati omnis eligendi aut and nvoluptatem doloribus vel accusantium quis pariatur molestiae porro eius odio et labore et velit aut'
    },
    {
      userId: 1,
      id: 4,
      title: 'Eum et est occaecati',
      body: 'ullam et saepe reiciendis voluptatem adipisci nsit amet autem assumenda provident rerum culpa quis hic commodi nesciunt rem tenetur doloremque ipsam iure quis sunt voluptatem rerum illo velit'
    },
    {
      userId: 2,
      id: 5,
      title: 'Qui est esse',
      body: 'est rerum tempore vitae nsequi sint nihil reprehenderit dolor beatae ea dolores neque nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis nqui aperiam non debitis possimus qui neque nisi nulla'
    },
    {
      userId: 2,
      id: 6,
      title: 'Molestias repellat',
      body: ' User 2 id_3 et iusto sed quo iure nvoluptatem occaecati omnis eligendi aut and nvoluptatem doloribus vel accusantium quis pariatur molestiae porro eius odio et labore et velit aut'
    },
    {
      userId: 1,
      id: 7,
      title: 'Eum 1-et est occaecati',
      body: 'ullam et saepe reiciendis voluptatem adipisci nsit amet autem assumenda provident rerum culpa quis hic commodi nesciunt rem tenetur doloremque ipsam iure quis sunt voluptatem rerum illo velit'
    },
  ]);

 
  return (
    <View  style={styles.container}>
      <View >
        <Text style={glStyles.title} > Головна Cторінка </Text>
      </View>
         
       <PagesList navigation ={ navigation} posts={posts}  />

    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f3e5f5',
  },
  
});
