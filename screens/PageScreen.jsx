 import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

// Global Styles file
import { glStyles } from '../styles/styles';


const PageScreen = ({ route }) => {
 
    return (
        <View >
          <View ><Text style={glStyles.title} > {route.params.title}</Text></View>
          <View style={styles.body}>
            <Text style={styles.text} > {route.params.body}</Text>
          </View>
        </View>
    );
 };

export default PageScreen;

const styles = StyleSheet.create({
  container: {
    
  },
  body:{
    padding: 20,
  },
  text:{
    textAlign:'justify',
    fontSize: 16,
  }
});
