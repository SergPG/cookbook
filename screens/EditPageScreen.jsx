 import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

// Global Styles file
import { glStyles } from '../styles/styles';


const EditPageScreen = ({ route }) => {
 
    return (
      
          <View ><Text style={glStyles.title} > EditPageScreen </Text></View>
    );
 };

export default EditPageScreen;

const styles = StyleSheet.create({
  container: {
    
  },
  body:{
    padding: 20,
  },
  text:{
    textAlign:'justify',
    fontSize: 16,
  }
});
