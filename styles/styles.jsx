import { StyleSheet, StatusBar  } from 'react-native';

export const glStyles = StyleSheet.create({
  main: {
    flex: 1,
    /* marginTop: StatusBar.currentHeight, */
    backgroundColor: '#3f0857',
  },
  title:{
     fontSize: 18,
     color: '#000',
     fontFamily: 'lobster-font',
     padding: 10,
     textAlign: 'center'
  },
});
